aiofiles==22.1.0
aiosqlite==0.19.0
anyio==3.6.2
argon2-cffi==21.3.0
argon2-cffi-bindings==21.2.0
arrow==1.2.3
association-measures==0.2.6
asttokens==2.2.1
attrs==23.1.0
Babel==2.12.1
backcall==0.2.0
bcrypt==3.2.0
beautifulsoup4==4.12.2
bleach==6.0.0
blinker==1.6.2
bokeh==3.1.1
Bottleneck==1.3.7
Brotli==1.0.9
certifi==2023.5.7
cffi==1.15.1
chardet==4.0.0
charset-normalizer==3.1.0
click==8.1.3
colorama==0.4.6
comm==0.1.3
contourpy==1.0.7
cryptography==3.4.8
cwb-ccc==0.11.8
cycler==0.11.0
dash==2.6.0
dash-bootstrap-components==1.4.1
dash-core-components==2.0.0
dash-html-components==2.0.0
dash-mantine-components==0.12.1
dash-renderjson==0.0.1
dash-table==5.0.0
dbus-python==1.2.18
debugpy==1.6.7
decorator==5.1.1
defer==1.0.6
defusedxml==0.7.1
distlib==0.3.6
distro==1.7.0
distro-info===1.1build1
duplicity==0.8.21
entrypoints==0.4
et-xmlfile==1.1.0
executing==1.2.0
fasteners==0.14.1
fastjsonschema==2.16.3
filelock==3.9.0
Flask==2.2.4
Flask-Compress==1.13
fonttools==4.39.3
fqdn==1.5.1
frontend==0.0.3
future==0.18.2
h11==0.14.0
httplib2==0.20.2
idna==3.4
importlib-metadata==4.6.4
ipykernel==6.22.0
ipython==8.12.0
ipython-genutils==0.2.0
ipywidgets==8.0.6
isoduration==20.11.0
itsdangerous==2.1.2
jedi==0.18.2
jeepney==0.7.1
Jinja2==3.1.2
json5==0.9.11
jsonpointer==2.3
jsonschema==4.17.3
jupyter-events==0.6.3
jupyter-server==1.24.0
jupyter-ydoc==0.2.4
jupyter_client==7.4.1
jupyter_core==5.3.0
jupyter_server_fileid==0.9.0
jupyter_server_terminals==0.4.4
jupyter_server_ydoc==0.8.0
jupyterlab==3.6.3
jupyterlab-pygments==0.2.2
jupyterlab-widgets==3.0.7
jupyterlab_server==2.22.1
keyring==23.5.0
kiwisolver==1.4.4
language-selector==0.1
launchpadlib==1.10.16
lazr.restfulclient==0.14.4
lazr.uri==1.0.6
linkify-it-py==2.0.2
lockfile==0.12.2
louis==3.20.0
macaroonbakery==1.3.1
Mako==1.1.3
Markdown==3.4.3
markdown-it-py==2.2.0
MarkupSafe==2.1.2
matplotlib==3.7.1
matplotlib-inline==0.1.6
mdit-py-plugins==0.3.5
mdurl==0.1.2
mistune==2.0.5
monotonic==1.6
more-itertools==8.10.0
nbclassic==0.5.5
nbclient==0.7.3
nbconvert==7.3.1
nbformat==5.8.0
nest-asyncio==1.5.6
netifaces==0.11.0
notebook==6.5.4
notebook_shim==0.2.3
numexpr==2.8.4
numpy==1.24.3
oauthlib==3.2.0
olefile==0.46
openpyxl==3.0.9
packaging==23.1
pandas==2.0.1
pandocfilters==1.5.0
panel==1.0.2
param==1.13.0
paramiko==2.9.3
parso==0.8.3
pexpect==4.8.0
pickleshare==0.7.5
Pillow==9.0.1
pipenv==2023.3.20
platformdirs==3.0.0
plotly==5.14.1
progress==1.6
prometheus-client==0.16.0
prompt-toolkit==3.0.38
protobuf==3.12.4
psutil==5.9.5
ptyprocess==0.7.0
pure-eval==0.2.2
pycairo==1.20.1
pycparser==2.21
pycups==2.0.1
Pygments==2.15.1
PyGObject==3.42.1
PyJWT==2.6.0
pymacaroons==0.13.0
PyMuPDF==1.22.2
PyNaCl==1.5.0
pyparsing==2.4.7
pyRFC3339==1.1
pyrsistent==0.19.3
python-apt==2.4.0+ubuntu1
python-arango==7.5.2
python-dateutil==2.8.2
python-debian===0.1.43ubuntu1
python-json-logger==2.0.7
pytz==2023.3
pyviz-comms==2.2.1
pyxdg==0.27
PyYAML==6.0
pyzmq==25.0.2
reportlab==3.6.8
requests==2.30.0
requests-toolbelt==1.0.0
rfc3339-validator==0.1.4
rfc3986-validator==0.1.1
rich==13.3.5
scipy==1.10.1
seaborn==0.12.2
SecretStorage==3.3.1
Send2Trash==1.8.0
six==1.16.0
sniffio==1.3.0
soupsieve==2.4.1
stack-data==0.6.2
starlette==0.26.1
systemd-python==234
tenacity==8.2.2
#terminado==0.17.1
tinycss2==1.2.1
tomli==2.0.1
tornado==6.3
tqdm==4.65.0
traitlets==5.9.0
typer==0.9.0
typing_extensions==4.5.0
tzdata==2023.3
ubuntu-advantage-tools==8001
ubuntu-drivers-common==0.0.0
uc-micro-py==1.0.2
ufw==0.36.1
unattended-upgrades==0.1
Unidecode==1.3.6
uri-template==1.2.0
urllib3==2.0.2
usb-creator==0.3.7
uvicorn==0.22.0
virtualenv==20.19.0
virtualenv-clone==0.5.7
voila==0.4.0
wadllib==1.3.6
wcwidth==0.2.6
webcolors==1.13
webencodings==0.5.1
websocket-client==1.5.1
websockets==11.0.3
Werkzeug==2.3.4
widgetsnbextension==4.0.7
xdg==5
xkit==0.0.0
xyzservices==2023.5.0
y-py==0.5.9
ypy-websocket==0.8.2
zipp==1.0.0
-i https://pypi.org/simple
alabaster==0.7.13 ; python_version >= '3.6'
astroid==2.11.7 ; python_full_version >= '3.6.2'
attrs==22.2.0 ; python_version >= '3.6'
babel==2.11.0 ; python_version >= '3.6'
bleach==6.0.0 ; python_version >= '3.7'
certifi==2022.12.7 ; python_version >= '3.6'
cffi==1.15.1
charset-normalizer==3.0.1 ; python_version >= '3.6'
colorama==0.4.6 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6'
coverage[toml]==7.1.0 ; python_version >= '3.7'
cryptography==39.0.1 ; python_version >= '3.6'
cython==0.29.30
dill==0.3.6 ; python_version >= '3.7'
docutils==0.18.1 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'
enthought-sphinx-theme==0.7.1
exceptiongroup==1.1.0
idna==3.4 ; python_version >= '3.5'
imagesize==1.4.1 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
importlib-metadata==6.0.0 ; python_version < '3.10'
iniconfig==2.0.0 ; python_version >= '3.7'
isort==5.11.4 ; python_full_version >= '3.7.0'
jaraco.classes==3.2.3 ; python_version >= '3.7'
jeepney==0.8.0 ; sys_platform == 'linux'
jinja2==3.1.2 ; python_version >= '3.7'
keyring==23.13.1 ; python_version >= '3.7'
lazy-object-proxy==1.9.0 ; python_version >= '3.7'
markupsafe==2.1.2 ; python_version >= '3.7'
mccabe==0.7.0 ; python_version >= '3.6'
more-itertools==9.0.0 ; python_version >= '3.7'
packaging==23.0 ; python_version >= '3.7'
pkginfo==1.9.6 ; python_version >= '3.6'
platformdirs==2.6.2 ; python_version >= '3.7'
pluggy==1.0.0 ; python_version >= '3.6'
py-cpuinfo==9.0.0
pycparser==2.21
pygments==2.14.0 ; python_version >= '3.6'
pylint==2.13.9
pyperclip==1.8.2
pytest==7.2.0
pytest-benchmark==4.0.0
pytest-cov==3.0.0
pytz==2022.7.1
readme-renderer==37.3 ; python_version >= '3.7'
requests==2.28.2 ; python_version >= '3.7' and python_version < '4'
requests-toolbelt==0.10.1 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
rfc3986==2.0.0 ; python_version >= '3.7'
secretstorage==3.3.3 ; sys_platform == 'linux'
setuptools==65.5.1
six==1.16.0 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
snowballstemmer==2.2.0
sphinx==5.0.0
sphinxcontrib-applehelp==1.0.4 ; python_version >= '3.8'
sphinxcontrib-devhelp==1.0.2 ; python_version >= '3.5'
sphinxcontrib-htmlhelp==2.0.0 ; python_version >= '3.6'
sphinxcontrib-jsmath==1.0.1 ; python_version >= '3.5'
sphinxcontrib-qthelp==1.0.3 ; python_version >= '3.5'
sphinxcontrib-serializinghtml==1.1.5 ; python_version >= '3.5'
tabulate==0.8.9
tomli==2.0.1 ; python_version < '3.11'
tqdm==4.64.1 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
twine==3.7.1
typing-extensions==4.4.0 ; python_version < '3.10'
urllib3==1.26.14 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4, 3.5'
webencodings==0.5.1
wrapt==1.14.1 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'
zipp==3.11.0 ; python_version >= '3.7'
association-measures==0.2.6
bottleneck==1.3.6
numexpr==2.8.4
numpy==1.24.1 ; python_version >= '3.8'
pandas==1.5.3
python-dateutil==2.8.2 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
pyyaml==6.0
scipy==1.10.0 ; python_version < '3.12' and python_version >= '3.8'
unidecode==1.3.6
wheel==0.38.4
